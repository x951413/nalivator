#include <avr/sleep.h>
#include <avr/power.h>


//Пины реле(подача воды)
const int RELAY_PINS[2] = {4, 3};
//Пины кнопок запуска подачи воды
const int BUTTON_PINS[2] = {7, 5};
//Пины для считывания показаний с расходомера PF3W540-F04-1T-R
const int RASKHODOMER_PINS[2] = {A1, A2};
//Пины для считывания с потенциометров требуемого количества литров
const int TARGET_LITERS_PINS[2] = {A0, A3};

//частота проверки расходомера, Герц
const int FREQ = 1000;

//Минимальное/максимальное показание потенциометра, в вольтах
const float MIN_POT_VAL_VOLTS = 0;
const float MAX_POT_VAL_VOLTS = 5;
//Минимальное/максимальное показание потенциометра, в литрах
const float MIN_POT_VAL_LITERS = 4;
const float MAX_POT_VAL_LITERS = 6;
//Минимальное/максимальное показание расходомера, в вольтах
const float MIN_VAL_VOLTS = 1.5;
const float MAX_VAL_VOLTS = 5;
//const float ABS_MIN_VAL_VOLTS = 1.4;
//Минимальное/максимальное показание расходомера, в литрах/минуту
const float MIN_VAL_LM = 5;
const float MAX_VAL_LM = 40;


//(16*10^6) / (1000*1) - 1 = 15999
//(16*10^6) / (freq*prescaler) - 1 (must be <65536)
const int TIMER_VALUE = 16000000 / FREQ - 1;

//Минимальное/максимальное показание расходомера, в микролитрах за T(T = 1/FREQ секунд)
const float MIN_VAL_ULT = (MIN_VAL_LM * 1000 * 1000) / 60 / FREQ; //83,(3)
const float MAX_VAL_ULT = (MAX_VAL_LM * 1000 * 1000) / 60 / FREQ; //666,(6)

//Минимальное/максимальное показание расходомера, в отсчётах АЦП
const int MIN_VAL = MIN_VAL_VOLTS / 5 * 1024;
const int MAX_VAL = MAX_VAL_VOLTS / 5 * 1024;
//Значение, ниже которого расход воды = 0
//const int ABS_MIN_VAL = ABS_MIN_VAL_VOLTS / 5 * 1024;

const float COEFF = (MAX_VAL_ULT - MIN_VAL_ULT) / (MAX_VAL - MIN_VAL);

const unsigned char STATE_WAIT = 0;
const unsigned char STATE_NALIV = 1;
const int TIMEOUT=2000;//ms

float targetLitersArray[2];
float currentLitersArray[2], currentRaskhodomers[2];
unsigned long int allValuesArray[2] = {0, 0};
unsigned long int measureCountArray[2] = {0, 0};
unsigned char currentStatesArray[2] = {STATE_WAIT, STATE_WAIT};
unsigned long lastMillisArray[2] = {0, 0};
//bool test = true;

void timerSetup() {
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register
  OCR1A = TIMER_VALUE;// = (16*10^6) / (freq*prescaler) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12, CS11, CS10 bits for prescaler
  //0 0 1 : 1
  //0 1 0 : 8
  //0 1 1 : 64
  //1 0 0 : 256
  //1 0 1 : 1024
  TCCR1B |= (0 << CS12) | (0 << CS11) | (1 << CS10);
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);
}

void startNaliv(int num) {
  measureCountArray[num] = 0;
  allValuesArray[num] = 0;
  digitalWrite(RELAY_PINS[num], HIGH);
  getTargetLiters(num);
  Serial.print("pot value: ");
  Serial.println(targetLitersArray[num]);
}

void endNaliv(int num) {
  digitalWrite(RELAY_PINS[num], LOW);
  measureCountArray[num] = 0;
  allValuesArray[num] = 0;
}

bool makeNaliv(int num) {
  int val = analogRead(RASKHODOMER_PINS[num]);
  if(val >= MIN_VAL) {
    allValuesArray[num] += (val - MIN_VAL);
    measureCountArray[num]++;
    currentLitersArray[num] = COEFF*allValuesArray[num] + MIN_VAL_ULT*measureCountArray[num];
    if(currentLitersArray[num] >= targetLitersArray[num]) {
      return true;
    }
  }
  return false;
}

void checkRaskhod(int num) {
  if(currentStatesArray[num] == STATE_WAIT) {
    if( (digitalRead(BUTTON_PINS[num]) == LOW) && ((millis()-lastMillisArray[num])>TIMEOUT) ) {
      lastMillisArray[num] = millis();
      startNaliv(num);
      currentStatesArray[num] = STATE_NALIV;
    }
  }
  if(currentStatesArray[num] == STATE_NALIV) {
    bool isEnoughLiters = makeNaliv(num);
    if(isEnoughLiters ||
      ( (digitalRead(BUTTON_PINS[num]) == LOW) && ((millis()-lastMillisArray[num])>TIMEOUT) )
      ) {
      lastMillisArray[num] = millis();
      endNaliv(num);
      currentStatesArray[num] = STATE_WAIT;
    }
  }
}

ISR(TIMER1_COMPA_vect) {
  checkRaskhod(0);
  checkRaskhod(1);

//  if(!isEnd) {
//    digitalWrite(4, test);
//    test = !test;
//  }
}

void getTargetLiters(int num) {
  int val = analogRead(TARGET_LITERS_PINS[num]);
  const int minVal = MIN_POT_VAL_VOLTS / 5 * 1024;
  const int maxVal = MAX_POT_VAL_VOLTS / 5 * 1024;
  if(val < minVal) {
    val = minVal;
  }
  float targetLiters = (val - minVal) * ((MAX_POT_VAL_LITERS - MIN_POT_VAL_LITERS) / (maxVal - minVal)) + MIN_POT_VAL_LITERS;
  targetLiters *= 1000000; //литры в микролитры
  targetLitersArray[num] = targetLiters;
}

void initPins() {
  pinMode(RELAY_PINS[0], OUTPUT);
  pinMode(RELAY_PINS[1], OUTPUT);
  pinMode(BUTTON_PINS[0], INPUT);
  pinMode(BUTTON_PINS[1], INPUT);
//  pinMode(4, OUTPUT);
  digitalWrite(RELAY_PINS[0], LOW);
  digitalWrite(RELAY_PINS[1], LOW);
  //Подтягивающие резисторы
  digitalWrite(BUTTON_PINS[0], HIGH);
  digitalWrite(BUTTON_PINS[1], HIGH);
}

void setup() {
  Serial.begin(9600);
  Serial.println("started");
  initPins();
  timerSetup();
}

void loop() {}
